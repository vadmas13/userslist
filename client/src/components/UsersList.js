import React from 'react'
import styles from "../assets/styles/Users.module.scss";
import Grid from "@material-ui/core/Grid";
import UserCart from "./UserCart";

const UsersList = ({data, limitValue, skipCount, counterUsers}) => {

        return (
            <Grid container justify="center" className={styles.container}>
                <Grid item>
                    {data.users.map(user => (
                        <UserCart user={user} key={user.id} limitValue={limitValue} counterUsers={counterUsers} skipCount={skipCount}/>
                    ))}
                </Grid>
            </Grid>
        )

}

export default UsersList