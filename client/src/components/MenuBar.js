import React from 'react'
import {Link} from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import styles from '../assets/styles/Users.module.scss'
import Container from "@material-ui/core/Container";

const MenuBar = () => {

    return (
        <AppBar position="static" className={styles.MenuBar}>
            <Container maxWidth="md">
                <Toolbar>
                    <Typography variant="h6" className={styles.MenuBar__link}>
                        <Link to={'/'}>Users list</Link>
                    </Typography>
                    <Typography variant="h6" className={styles.MenuBar__link}>
                        <Link to={'/create'}>Create user</Link>
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>
    )
}

export default MenuBar

