import React from 'react'
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grow from "@material-ui/core/Grow";
import styles from './../assets/styles/Users.module.scss'
import MuiAlert from "@material-ui/lab/Alert";

const FormUser = ({callback, setUserData, userData, errors, msg}) => {

    const handleChange = (e) => {
        setUserData({
            ...userData, [e.target.id]: e.target.value
        })
    }

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    return (
        <div className={styles.containerForm}>
            <h1 className="titleHeader">{msg ? msg :'Create user'} </h1>
            <form noValidate autoComplete="off" className={styles.containerForm__form}>
                <TextField id="name" value={userData.name} label="Name" className={styles.containerForm__input}
                           onChange={handleChange}/>
                <TextField id="email" value={userData.email} label="Email" className={styles.containerForm__input}
                           onChange={handleChange}/>
                <Button variant="contained" color="primary" className={styles.containerForm__button} onClick={callback}>
                    Submit
                </Button>
            </form>

            <div className={styles.errors}>
                {Object.keys(errors).length > 0 && Object.values(errors).map((error, i) => (
                    <Grow in={true} key={i}>
                        <Alert className={styles.errors__item} severity="error">{error}</Alert>
                    </Grow>
                ))}
            </div>
        </div>
    )
}

export default FormUser