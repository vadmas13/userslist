import React, {useContext, useState} from 'react'
import Button from "@material-ui/core/Button";
import {DeleteSweep} from "@material-ui/icons";
import Dialog from "@material-ui/core/Dialog";
import Container from "@material-ui/core/Container";
import styles from './../assets/styles/Users.module.scss'
import {useMutation} from "@apollo/react-hooks";
import gql from "graphql-tag";
import {FETCH_USERS_QUERY, USERS_COUNT_QUERY} from "../util/graphql";
import Tooltip from "@material-ui/core/Tooltip";
import {LIMIT_USERS_DEFAULT, SKIP_USERS_DEFAULT} from "../util/constants";
import {NotificationsContext} from "../context/notifications";



const DeleteButton = (props) => {

    const {userId, username, redirect, skipCount, limitValue, counterUsers} = props;
    const [open, setOpen] = useState(false);

    const { setMessage } = useContext(NotificationsContext);

    const [deleteUser] = useMutation(DELETE_USER_MUTATION, {
        variables: {
            id: userId
        },
        update(proxy, result) {
            let usersLength;
            if(!counterUsers){
                usersLength = proxy.readQuery({
                    query: USERS_COUNT_QUERY
                }).usersCount;
            }else usersLength = counterUsers;

            const data = proxy.readQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: limitValue ? limitValue : LIMIT_USERS_DEFAULT,
                    skip: skipCount ? skipCount : SKIP_USERS_DEFAULT
                }
            });
            proxy.writeQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: limitValue ? limitValue : LIMIT_USERS_DEFAULT,
                    skip: skipCount ? skipCount : SKIP_USERS_DEFAULT
                },
                data: {users: [...data.users.filter(user => user.id !== userId)]}
            })
            proxy.writeQuery({
                query: USERS_COUNT_QUERY,
                data: { usersCount: usersLength - 1}
            })
            setMessage(`Deleted user ${result.data.deleteUser.name}`);
            setOpen(false);
            if (redirect) redirect();
        }
    })


    return (
        <>
            <Tooltip title={`Delete ${username}`} arrow>
                <Button color="secondary" variant="contained" onClick={() => setOpen(true)}>
                    <DeleteSweep/>
                </Button>
            </Tooltip>
            <Dialog onClose={() => setOpen(false)} aria-labelledby="simple-dialog-title" open={open}>
                <Container maxWidth="sm" className={styles.DeleteButton__confirm}>
                    <p className={styles.DeleteButton__text}>Delete user {username}? </p>
                    <Button variant="contained" color="secondary" className={styles.DeleteButton__button}
                            onClick={deleteUser}>
                        Delete
                    </Button>
                    <Button variant="contained" color="primary" className={styles.DeleteButton__button}
                            onClick={() => setOpen(false)}>
                        Cancel
                    </Button>
                </Container>
            </Dialog>
        </>
    )
}

const DELETE_USER_MUTATION = gql`
    mutation deleteUser($id: ID!){
        deleteUser(id: $id){
            id name email
        }
    }
`


export default DeleteButton