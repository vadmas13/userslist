import React, {useContext} from 'react'
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import {NotificationsContext} from "../context/notifications";

const Notifications = () => {

    const {message, open, unsetMessage} = useContext(NotificationsContext);

    return(
        <Snackbar open={open} autoHideDuration={6000} onClose={() => unsetMessage()}>
            <Alert onClose={() => console.log('close')} severity="success">
                { message }
            </Alert>
        </Snackbar>
    )
}

export default Notifications