import React from 'react'
import Button from "@material-ui/core/Button";
import UpdateIcon from '@material-ui/icons/Update';
import styles from "../assets/styles/Users.module.scss";
import {Link} from "react-router-dom";
import Tooltip from "@material-ui/core/Tooltip";
import {
    createMuiTheme,
    MuiThemeProvider
} from "@material-ui/core/styles";


const theme = createMuiTheme({
    overrides: {
        MuiTooltip: {
            tooltip: {
                fontSize: "1rem"
            }
        }
    }
});

const UpdateButton = ({userId, username}) => {

    return (
        <Link to={`/update/${userId}`}>
            <MuiThemeProvider theme={theme}>
                <Tooltip title={`Update ${username}`} style={{fontSize: '20px'}} arrow>
                    <Button variant="contained" color="primary" className={styles.UpdateButton}>
                        <UpdateIcon/>
                    </Button>
                </Tooltip>
            </MuiThemeProvider>
        </Link>
    )
}

export default UpdateButton