import React from 'react'
import styles from "../assets/styles/Users.module.scss";
import Paper from "@material-ui/core/Paper";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import DeleteButton from "./DeleteButton";
import {Link} from "react-router-dom";
import UpdateButton from "./UpdateButton";


const UserCart = ({user, skipCount, limitValue, counterUsers}) => {

    return (
        <Paper className={styles.Users}>
            <ListItem style={{justifyContent: 'space-between'}}>
                <ListItemAvatar>
                    <Avatar>
                        {user.name[0].toUpperCase()}
                    </Avatar>
                </ListItemAvatar>
                <div className={styles.Users__info}>
                    <Link to={`/users/${user.id}`}>
                        <ListItemText className={styles.Users__text} primary={user.name}/>
                    </Link>
                </div>
                <div className={styles.Users__nav}>
                    <DeleteButton userId={user.id} username={user.name} skipCount={skipCount} limitValue={limitValue} counterUsers={counterUsers}/>
                    <UpdateButton userId={user.id} username={user.name} skipCount={skipCount} limitValue={limitValue}/>
                </div>
            </ListItem>
        </Paper>
    )
}

export default UserCart