import React from 'react'
import styles from "../assets/styles/Users.module.scss";
import Pagination from "@material-ui/lab/Pagination";
import Grid from "@material-ui/core/Grid";

const Paginate = ({counterUsers, dataLength, setCurrentPage, currentPage}) => {

        const countPages = Math.ceil( counterUsers / dataLength);

        return(
            <Grid container justify="center" className={styles.container}>
                <Pagination count={countPages}
                            page={currentPage}
                            onChange={(_, page) => setCurrentPage(page)}
                            color="primary"/>
            </Grid>
        )

}

export default Paginate