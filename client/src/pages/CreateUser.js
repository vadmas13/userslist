import React, {useContext, useState} from 'react'
import {useMutation} from "@apollo/react-hooks";
import gql from "graphql-tag";
import {FETCH_USERS_QUERY, USERS_COUNT_QUERY} from "../util/graphql";
import FormUser from "../components/FormUser";
import {LIMIT_USERS_DEFAULT, SKIP_USERS_DEFAULT} from "../util/constants";
import {NotificationsContext} from "../context/notifications";

const CreateUser = (props) => {

    const [userData, setUserData] = useState({name: '', email: ''})
    const [errors, setErrors] = useState({});

    const { setMessage } = useContext(NotificationsContext);

    const [createUser, {loading}] = useMutation(CREATE_USER_MUTATION, {
        variables: {
            ...userData
        },
        update(proxy, result){
            const data = proxy.readQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: LIMIT_USERS_DEFAULT,
                    skip: SKIP_USERS_DEFAULT
                }
            });
            const usersCount = proxy.readQuery({
                query: USERS_COUNT_QUERY
            });
            proxy.writeQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: LIMIT_USERS_DEFAULT,
                    skip: SKIP_USERS_DEFAULT
                },
                data: { users: [ result.data.createUser , ...data.users] }
            })
            proxy.writeQuery({
                query: USERS_COUNT_QUERY,
                data: { usersCount : usersCount.usersCount + 1 }
            });
            setMessage(`Created new user ${result.data.createUser.name}`);
            props.history.push(`/users/${result.data.createUser.id}`);
        },
        onError(err) {
            setErrors(err.graphQLErrors[0].extensions.errors)
        }
    })


    return <FormUser loading={loading} callback={createUser} errors={errors} setUserData={setUserData} userData={userData}/>





}

const CREATE_USER_MUTATION = gql`
    mutation createUser($name: String!, $email: String!){
        createUser( input: {name: $name, email: $email} ){
            id name email
        }

    }
`

export default CreateUser;