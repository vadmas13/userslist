import React from 'react'
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import styles from "../assets/styles/Users.module.scss";
import {useQuery} from "@apollo/react-hooks";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import DeleteButton from "../components/DeleteButton";
import ListItem from "@material-ui/core/ListItem";
import CircularProgress from "@material-ui/core/CircularProgress";
import {FETCH_USER_QUERY} from "../util/graphql";
import UpdateButton from "../components/UpdateButton";

const UserInfo = (props) => {
    const userId = props.match.params.id;

    const { data, loading } = useQuery(FETCH_USER_QUERY, {
        variables: {
            id: userId
        }
    })

    if(!loading) return(
        <Card className={styles.UserInfo}>
            <CardContent>
                <ListItem className={styles.UserInfo__container}>
                    <ListItemAvatar>
                        <Avatar className={styles.UserInfo__avatar}>
                            {data.user.name[0].toUpperCase()}
                        </Avatar>
                    </ListItemAvatar>
                    <div className={`${styles.Users__info} ${styles.UserInfo__info}`}>
                        <ListItemText className={styles.Users__text} primary="ID" secondary={data.user.id}/>
                        <ListItemText className={styles.Users__text} primary="Name" secondary={data.user.name}/>
                        <ListItemText className={styles.Users__text} primary="Email" secondary={data.user.email}/>
                    </div>
                    <div className={styles.Users__nav}>
                        <DeleteButton userId={data.user.id} username={data.user.name} redirect={() => props.history.push('/')}/>
                        <UpdateButton userId={data.user.id} username={data.user.name}/>
                    </div>
                </ListItem>
            </CardContent>
        </Card>
    );
    else return <CircularProgress className={styles.preloader}/>
}

export default UserInfo

