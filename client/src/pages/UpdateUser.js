import React, {useContext, useState} from 'react'
import {useMutation, useQuery} from "@apollo/react-hooks";
import {FETCH_USER_QUERY, FETCH_USERS_QUERY} from "../util/graphql";
import FormUser from "../components/FormUser";
import gql from "graphql-tag";
import {LIMIT_USERS_DEFAULT, SKIP_USERS_DEFAULT} from "../util/constants";
import {NotificationsContext} from "../context/notifications";

const UpdateUser = (props) => {

    const initState = {name: '', email: ''}
    const userId = props.match.params.id;
    const [errors, setErrors] = useState({});
    const [userData, setUserData] = useState(initState)

    const { setMessage } = useContext(NotificationsContext);

    const { data } = useQuery(FETCH_USER_QUERY, {
        variables: {
            id: userId
        }
    })

    if(data && (userData.name === '' && userData.email === '')){
        setUserData({...data.user});
    }


    const [updateUser, {loading}] = useMutation(UPDATE_USER_MUTATION, {
        variables: {
           ...userData, id: userId
        },
        update(proxy, result){
            const dataUsers = proxy.readQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: LIMIT_USERS_DEFAULT,
                    skip: SKIP_USERS_DEFAULT
                }
            });
            proxy.writeQuery({
                query: FETCH_USERS_QUERY,
                variables: {
                    limit: LIMIT_USERS_DEFAULT,
                    skip: SKIP_USERS_DEFAULT
                },
                data: { users: [ ...dataUsers.users.map(user => {
                        if(user.id === userId){
                            return {...result.data.updateUser}
                        }else return user
                    } )] }
            });
            setMessage(`Updated user ${result.data.updateUser.name}`)
            setUserData(initState);
            props.history.push(`/users/${result.data.updateUser.id}`);
        },
        onError(err) {
            setErrors(err.graphQLErrors[0].extensions.errors)
        }
    })


    return <FormUser loading={loading} callback={updateUser} errors={errors} setUserData={setUserData} userData={userData} msg={'Update user'}/>

}

const UPDATE_USER_MUTATION = gql`    
    mutation updateUser($id: ID!, $name: String, $email: String){
        updateUser(id: $id, input: { name: $name, email: $email}){
            id name email
        }
    }
`

export default UpdateUser