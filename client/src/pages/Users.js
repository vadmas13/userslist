import React, {useState} from 'react'
import Grid from "@material-ui/core/Grid";
import {useQuery} from "@apollo/react-hooks";
import CircularProgress from "@material-ui/core/CircularProgress";
import {FETCH_USERS_QUERY, USERS_COUNT_QUERY} from "../util/graphql";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import styles from '../assets/styles/Users.module.scss'
import SaveIcon from '@material-ui/icons/Save';
import Button from "@material-ui/core/Button";
import UsersList from "../components/UsersList";
import Paginate from "../components/Paginate";
import {LIMIT_USERS_DEFAULT} from "../util/constants";



const Users = () => {

    const [limitValue, setLimitValue] = useState(LIMIT_USERS_DEFAULT)
    const [limitSlider, setLimitSlider] = useState(LIMIT_USERS_DEFAULT)
    const [currentPage, setCurrentPage] = useState();


    let skipCount = currentPage ? (currentPage - 1) * limitValue : 0;


    const countUser = useQuery(USERS_COUNT_QUERY);
    const {data, loading} = useQuery(FETCH_USERS_QUERY, {
        variables: {
            limit: limitValue,
            skip: skipCount
        }
    })


    let usersData = data ? data : null


    if (countUser.data && usersData) {
        return (
            <>
                <h1 className="titleHeader">Users list</h1>
                <Grid container justify="space-around" className={styles.container}>
                    <Typography variant="h6" gutterBottom>
                        {`Common count Users: ${countUser.data.usersCount}`}
                    </Typography>
                    <Grid item xs={10}>
                        <Slider
                            value={limitSlider}
                            aria-labelledby="discrete-slider-small-steps"
                            step={1}
                            marks
                            min={1}
                            onChange={(_, value) => setLimitSlider(value)}
                            max={countUser.data.usersCount}
                            valueLabelDisplay="auto"
                        />
                    </Grid>
                    <Grid item xs={2} className={styles.container__button}>
                        <Button
                            variant="contained"
                            color="primary"
                            size="small"
                            startIcon={<SaveIcon/>}
                            onClick={() => setLimitValue(limitSlider)}
                        >
                            Save
                        </Button>
                    </Grid>
                </Grid>
                { !loading ? (
                    <>
                        <UsersList data={usersData} limitValue={limitValue} skipCount={skipCount} counterUsers={countUser.data.usersCount}/>
                        <Paginate counterUsers={countUser.data.usersCount}
                                  setCurrentPage={setCurrentPage}
                                  currentPage={currentPage}
                                  dataLength={limitValue}/>
                    </>
                ) : <CircularProgress className={styles.preloader}/>}

                }
            </>
        )
    } else {
        return <CircularProgress className={styles.preloader}/>
    }


}


export default Users