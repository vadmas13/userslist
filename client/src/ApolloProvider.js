import React from 'react'
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";
import App from "./App";

const client = new ApolloClient({
    uri: 'http://localhost:5000'
})

export default (
    <ApolloProvider client={client}>
        <App />
    </ApolloProvider>
)