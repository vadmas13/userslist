import gql from "graphql-tag";

export const FETCH_USERS_QUERY = gql`
    query users($limit: Int, $skip: Int){
        users(limit: $limit, skip: $skip){
            id name email
        }
    }
`

export const FETCH_USER_QUERY = gql`
    query user($id: ID!){
        user(id: $id){
            id
            name
            email
        }
    }
`


export const USERS_COUNT_QUERY = gql`    
    query usersCount{
        usersCount
    }
`