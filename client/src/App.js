import React from 'react';
import './App.css';
import Users from "./pages/Users";
import {BrowserRouter as Router, Route} from "react-router-dom";
import CreateUser from "./pages/CreateUser";
import UserInfo from "./pages/UserInfo";
import MenuBar from "./components/MenuBar";
import UpdateUser from "./pages/UpdateUser";
import {NotificationProvider} from "./context/notifications";
import Notifications from "./components/Notifications";

const App = () => {
    return (
        <NotificationProvider>
            <div className="App">
                <Router>
                    <MenuBar/>
                    <Route exact path={'/'} component={Users}/>
                    <Route exact path={'/create'} component={CreateUser}/>
                    <Route exact path={'/update/:id'} component={UpdateUser}/>
                    <Route exact path={'/users/:id'} component={UserInfo}/>
                    <Notifications/>
                </Router>
            </div>
        </NotificationProvider>
    );
}

export default App;
