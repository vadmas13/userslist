const User = require('./../../models/Users');
const {UserInputError} = require('apollo-server')
const {validateUserFields} = require('./../../utils/validators')

module.exports = {
    Query: {
        async usersCount(){
            const users = await User.find();
            return users.length
        },
        async users(_, {limit, skip}) {
            try {
                const users = await User.find().skip(skip).limit(limit);
                return users
            } catch (e) {
                throw new Error(e);
            }
        },
        async user(_, {id}) {
            try {
                const user = await User.findById(id);
                if (user) {
                    return user
                } else {
                    throw new Error('Пользователь не найден')
                }
            } catch (e) {
                throw new Error(e)
            }
        }
    },
    Mutation: {
        async createUser(_, {input: {email, name}}) {

            const {valid, errors} = validateUserFields(email, name);

            if (!valid) {
                throw new UserInputError('Errors fields', {errors})
            }

            const newUser = new User({
                name,
                email
            })

            const user = await newUser.save();

            return {
                ...user._doc,
                id: user._id
            };
        },
        async updateUser(_, {id, input: { name, email }}) {

            const {valid, errors} = validateUserFields(email, name);

            if (!valid) {
                throw new UserInputError('Errors fields', {errors})
            }

            let user = await User.findById(id)

            if (user) {
                user.name = name;
                user.email = email;

                await user.save();

                return user
            } else {
                errors.general = 'Пользователь не найден'
                throw new UserInputError('Пользователь не найден', {errors});
            }

        },
        async deleteUser(_, { id } ){

            const user = User.findOneAndRemove({ _id: id })

            return user;
        }
    }
}