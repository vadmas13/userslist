module.exports.validateUserFields = (email, name) => {
    let errors = {}

    if(email.trim() === ''){
        errors.email = 'email не может быть пустым'
    }else{
        const regEx = /^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;
        if(!email.match(regEx)){
            errors.email = 'Не валидный email';
        }
    }

    if(name.trim() === ''){
        errors.name = 'Логин не заполнен';
    }

    return{
        errors,
        valid: Object.keys(errors).length < 1
    }
}